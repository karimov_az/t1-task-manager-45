package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITokenService {

    @NotNull
    String getToken();

    void setToken(String token);

}
