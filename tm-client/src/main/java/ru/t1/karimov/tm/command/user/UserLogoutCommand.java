package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User logout.";

    @NotNull
    public static final String NAME = "logout";

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOGOUT");
        @NotNull UserLogoutRequest request = new UserLogoutRequest(getToken());
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
